from var import var
from const import const
from plus import plus
from prod import prod
from pwr import pwr
from maker import make_const, make_pwr, make_pwr_expr
from deriv import deriv, const_deriv, pwr_deriv, plus_deriv, prod_deriv
import math

fex = make_pwr('x', 1.0)
print(fex)
# (x^1.0)
#drv = deriv(fex)
#print drv
# (1.0*(x^0.0))

