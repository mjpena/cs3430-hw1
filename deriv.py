#!/usr/bin/python

####################################
# Melanie Pena
# A02072922
####################################

from var import var
from const import const
from pwr import pwr
from prod import prod
from plus import plus
from maker import make_const, make_pwr, make_pwr_expr
import math

def deriv(expr):
    if isinstance(expr, const):
        return const_deriv(expr)
    elif isinstance(expr, pwr):
        return pwr_deriv(expr)
    elif isinstance(expr, prod):
        return prod_deriv(expr)
    elif isinstance(expr, plus):
        return plus_deriv(expr)
    else:
        raise Exception('deriv:' + repr(expr))

# the derivative of a consant is 0.
def const_deriv(c):
    assert isinstance(c, const)
    return const(val=0.0)

def plus_deriv(s):
    # your code here
    assert isinstance(s, plus)
    e1 = s.get_elt1()
    e2 = s.get_elt2()
    d1 = deriv(e1)
    d2 = deriv(e2)
    final = plus(elt1=d1, elt2=d2)
    return final

def pwr_deriv(p):
    assert isinstance(p, pwr)
    b = p.get_base()    # variable
    d = p.get_deg()     # exponent
    if isinstance(b, var):
        if isinstance(d, const):
            # your code here
            # example: x^1.0
            exp = const.add(d, make_const(-1.0))
            drv = prod(mult1 = d, mult2=make_pwr(str(b), exp))
            return drv
        else:
            raise Exception('pwr_deriv: case 1: ' + str(p))
    if isinstance(b, pwr):
        if isinstance(d, const):
            # your code here
            # example: (x^2.0)^2.0
            exp = const.add(d, make_const(-1.0))
            out_drv = prod(mult1=d, mult2=make_pwr(str(b), exp))
            in_drv = deriv(b)
            drv = prod(mult1=out_drv, mult2=in_drv)
            return drv
        else:
            raise Exception('pwr_deriv: case 2: ' + str(p))
    elif isinstance(b, plus):
        if isinstance(d, const):
            # your code here
            # example: ((((x^3.0)+3.0)^4.0)
            exp = const.add(d, make_const(-1.0))
            out_drv = prod(mult1=d, mult2=make_pwr(str(b), exp))
            in_drv = deriv(b)
            drv = prod(mult1=out_drv, mult2=in_drv)
            return drv
        else:
            raise Exception('pwr_deriv: case 3: ' + str(p))
    elif isinstance(b, prod):
        if isinstance(d, const):
            # your code here
            # example: ((5.0*(x^10.0))^4.0)
            exp = const.add(d, make_const(-1.0))
            out_drv = prod(mult1=d, mult2=make_pwr(str(b), exp))
            in_drv = deriv(b)
            drv = prod(mult1=out_drv, mult2=in_drv)
            return drv
        else:
            raise Exception('pwr_deriv: case 4: ' + str(p))
    else:
        raise Exception('power_deriv: case 5: ' + str(p))

def prod_deriv(p):
    assert isinstance(p, prod)
    m1 = p.get_mult1()
    m2 = p.get_mult2()
    if isinstance(m1, const):
        if isinstance(m2, const):
            # your code here
            # example: (5.0*2.0))
            return make_const(0.0)
        elif isinstance(m2, pwr):
            # your code here
            # example: (5.0*(x^2.0))
            m2_drv = deriv(m2)
            drv = prod(mult1=m1, mult2=m2_drv)
            return drv
        elif isinstance(m2, plus):
            # your code here
            # example: (5.0*(3.0+(x^3.0)))
            m2_drv = deriv(m2)
            drv = prod(mult1=m1, mult2=m2_drv)
            return drv
        elif isinstance(m2, prod):
            # your code here
            # example: (2.0*(3.0*(x^3.0)))
            m2_drv = deriv(m2)
            drv = prod(mult1=m1, mult2=m2_drv)
            return drv
        else:
            raise Exception('prod_deriv: case 0' + str(p))
    elif isinstance(m1, plus):
        if isinstance(m2, const):
            # your code here
            # example: ((3.0+(x^3.0))*5.0)
            m1_drv = deriv(m1)
            drv = prod(mult1=m1_drv, mult2=m2)
            return drv
        else:
            raise Exception('prod_deriv: case 1:' + str(p))
    elif isinstance(m1, pwr):
        if isinstance(m2, const):
            # your code here
            # example: ((x^2.0)*5.0)
            m1_drv = deriv(m1)
            drv = prod(mult1=m1_drv, mult2=m2)
            return drv
        else:
            raise Exception('prod_deriv: case 2:' + str(p))
    elif isinstance(m1, prod):
        if isinstance(m2, const):
            # your code here
            # example: ((3.0*(x^3.0))*2.0)
            m1_drv = deriv(m1)
            drv = prod(mult1=m1_drv, mult2=m2)
            return drv
        else:
            raise Exception('prod_deriv: case 3:' + str(p))
    else:
       raise Exception('prod_deriv: case 4:' + str(p))



'''
## test cases ##
fex = plus(elt1=make_const(1.0), elt2=make_const(1.0))
print(fex)
drv = deriv(fex)
print(drv)

fex = make_pwr('x', 1.0)
print(fex)
# (x^1.0)
drv = deriv(fex)
print(drv)
# (1.0*(x^0.0))

fex = make_pwr('z', 5)
print(fex)
# (x^5.0)
drv = deriv(fex)
print(drv)
# (5*(x^4.0))

fex = prod(mult1=make_const(5.0), mult2=make_pwr('x',2.0))
print(fex)
drv = deriv(fex)
print(drv)
# (5.0*(2.0*(x^1.0)))

fex = prod(mult1=make_pwr('x', 2.0), mult2=make_const(5.0))
print(fex)
# ((x^2.0)*5.0)
# drv = deriv(fex)
print(drv)
# (5.0*(2.0*(x^1.0)))

prd = prod(mult1=make_const(5.0), mult2=make_pwr('x', 10.0))
fex = make_pwr_expr(prd, 4.0)
print(fex)
#((5.0*(x^10.0))^4.0)
drv = deriv(fex)
print(drv)
#((4.0*((5.0*(x^10.0))^3.0))*(5.0*(10.0*(x^9.0))))

fex = make_pwr_expr(plus(elt1=make_pwr('x', 3.0), elt2=make_const(3.0)), 4.0)
print(fex)
# (((x^3.0)+3.0)^4.0)
drv = deriv(fex)
print(drv)
# ((4.0*(((x^3.0)+3.0)^3.0))*((3.0*(x^2.0))+0.0))

fex = plus(elt1=make_pwr('x', 2.0), elt2=make_pwr('x', 1.0))
fex2 = plus(elt1=fex, elt2=make_const(-100.0))
print(fex2)
# (((x^2.0)+(x^1.0))+-100.0)
drv = deriv(fex2)
print(drv)
# (((2.0*(x^1.0))+(1.0*(x^0.0)))+0.0)

fex = prod(make_const(5.0), make_const(2.0))
print(fex)
# (5.0*2.0)
drv = deriv(fex)
print(drv)
# 0.0

fex = prod(mult1=make_const(5.0),mult2=plus(elt1=make_const(3.0), elt2=make_pwr('x', 3.0)))
print(fex)
#(5.0*(3.0+(x^3.0)))
drv = deriv(fex)
print(drv)

fex = prod(mult1= make_const(2.0), mult2=prod(mult1=make_const(3.0), mult2=make_pwr('x', 3.0)))
print(fex)
#(2.0*(3.0*(x^3.0)))
drv = deriv(fex)
print(drv)

fex = prod(mult1=plus(elt1=make_const(3.0), elt2=make_pwr('x', 3.0)), mult2=(make_const(5.0)))
print(fex)
#((3.0+(x^3.0))*5.0)
drv = deriv(fex)
print(drv)

fex = prod(mult1=make_pwr('x', 2.0), mult2=make_const(5.0))
print(fex)
#((x^2.0)*5.0)
drv = deriv(fex)
print(drv)

fex = prod(mult1=prod(mult1=make_const(3.0), mult2=make_pwr('x', 3.0)), mult2=make_const(2.0))
print(fex)
#((3.0*(x^3.0))*2.0)
drv = deriv(fex)
print(drv)
'''